extends Area2D

export (Vector2) var rebote=Vector2(0,-400)

func _ready():
	connect("body_entered",self,"hacerRebotar")

func hacerRebotar(cuerpo):
	if cuerpo.has_method("rebotar"):
		Sfx.get_node("rebotador").play()
		cuerpo.rebotar(rebote)
		cambiarAnim("jump")
		yield($spr,"animation_finished")
		cambiarAnim("default")
		

func cambiarAnim(anim):
	$spr.animation=anim
	$sombra.animation=anim
