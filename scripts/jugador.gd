extends KinematicBody2D

var velocidad=Vector2.ZERO
var vel=100
var gravedad=10
var friccion=1

var ui



func _ready():
	
	ui=get_tree().get_nodes_in_group("ui")[0]

func _physics_process(delta):
	
	if Juego.so=="Android":
		if (Input.is_action_just_pressed("ui_left") or Input.is_action_just_pressed("ui_right")):
			Input.vibrate_handheld(100)
	
	if global_position.y>$Camera2D.limit_bottom:
		Transiciones.reiniciarFade()
	
	velocidad.y+=gravedad
	
	if velocidad.x>0:
		flipH(false)
	elif velocidad.x<0:
		flipH(true)
	
	
	
	
	
	if is_on_floor():
		
		$sombra.z_index=-1
		if Input.is_action_pressed("ui_right") :
			velocidad.x=vel
			
			
		elif Input.is_action_pressed("ui_left") :
			velocidad.x=-vel
			
			
		else:
			velocidad.x=0
		if velocidad.x!=0:
			cambiarAnim("walk")
		else:
			cambiarAnim("idle")
		
		
	else:
		if Input.is_action_pressed("ui_right") :
			velocidad.x=min(velocidad.x+vel/16,vel)
			
		elif Input.is_action_pressed("ui_left") :
			
			
			velocidad.x=max(velocidad.x-vel/16,-vel)
			
		else:
			if velocidad.x>0:
				velocidad.x=max(velocidad.x-friccion,0)
			elif velocidad.x<0:
				velocidad.x=min(velocidad.x+friccion,0)
		
		$sombra.z_index=0
		cambiarAnim("jump")
	
	velocidad=move_and_slide(velocidad,Vector2.UP)

func rebotar(rebote):
	velocidad=rebote
	

func cambiarAnim(anim):
	$spr.animation=anim
	$sombra.animation=anim

func flipH(flip):
	$spr.flip_h=flip
	$sombra.flip_h=flip

