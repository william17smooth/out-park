extends Area2D

var rebote=Vector2(0,-400)
var tiempo=randi()

enum tipoDeGlobo{normal,explosivo,item}

export (tipoDeGlobo) var tipo=tipoDeGlobo.normal

func _ready():
	if tipo==tipoDeGlobo.explosivo:
		$anim.play("explosivo_idle")
	else:
		var color=Color(randf(),randf(),randf())
		$spr.modulate=color

	

	connect("body_entered",self,"hacerRebotar")

func _physics_process(delta):
	
	
	tiempo+=delta
	translate(Vector2(0,(sin(tiempo)*8*delta)))

func hacerRebotar(cuerpo):
	if cuerpo.has_method("rebotar"):
		cuerpo.rebotar(rebote)
		
		match tipo:
			tipoDeGlobo.normal:
				rebotar()
			tipoDeGlobo.explosivo:
				explotar()
		
		
		
		
		

func explotar():
	Sfx.get_node("explosion_globo").play()
	queue_free()


func rebotar():
	
	Sfx.get_node("rebote").play()
	
	var tween = get_node("tween")
	tween.interpolate_property(self, "global_position",
	global_position, global_position+Vector2(0, 16), .1,
	Tween.TRANS_BOUNCE, Tween.EASE_IN_OUT)
	tween.start()

	yield(tween,"tween_completed")

	tween.interpolate_property(self, "global_position",
	global_position, global_position+Vector2(0, -16), .2,
	Tween.TRANS_BOUNCE, Tween.EASE_IN_OUT)
	tween.start()
