extends CanvasLayer


func _ready():
	pass

func cambiarFade(escena):
	$anim.play("fade_out")
	yield($anim,"animation_finished")
	
	get_tree().change_scene(escena)
	
	$anim.play("fade_in")
	
	

func reiniciarFade():
	$anim.play("fade_out")
	yield($anim,"animation_finished")
	
	get_tree().reload_current_scene()
	
	$anim.play("fade_in")
	
	
