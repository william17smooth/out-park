extends Node

var volume=100

func _ready():
	volume=range_lerp(volume,0,100,-80,0)
	
	for sfx in get_children():
		sfx.volume_db=volume
	
