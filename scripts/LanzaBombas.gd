extends Position2D

var x=0
var player
var begin
var end
var distancia

func _ready():
	player=get_parent().get_node("jugador")
	begin=global_position
	end=player.global_position
	distancia=abs(begin.x-end.x)/2
	x=begin.x
	

func _process(delta):
	distancia=abs(begin.x-end.x)/2
	x+=delta
	var movx=range_lerp(x,begin.x,end.x,-distancia,distancia)
	var y=-(x*x)
	translate(Vector2(movx,y)*delta)
	
